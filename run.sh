#!/usr/bin/env bash

# build all services
docker-compose build

# get all services and run one after one
services="$(docker-compose ps --services)"
for x in $services; do
   docker-compose up --force-recreate "$x" 2>&1 | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g" | tee "results/$x.log"
done

# rce-weblogs-bypass

According to http://httpd.apache.org/docs/current/logs.html#accesslog and https://docs.nginx.com/nginx/admin-guide/monitoring/logging/#setting-up-the-access-log __access_log__ entries are created only for **processed** requests. So I wondered if there is a way to interrupt request processing and leave  no traces at all, even in __error_log__. This is important especially for forensic analysis, when site is vulnerable to __Remote Code Execution__.

This repository contains code base for finding possibility of making malicious request, which will not leave any traces.

## Environments

There are multiple ways to run webserver with PHP backend, so I tested most popular ones:

- Apache (prefork MPM) + mod_php
- Apache (prefork MPM) + FastCGI
- Apache (worker MPM) + FastCGI
- Apache (event MPM) + FastCGI
- Apache (prefork MPM) + FPM
- Apache (worker MPM) + FPM
- Apache (event MPM) + FPM
- Nginx + FastCGI
- Nginx + FPM

For simplicity every environment is created as docker container.

**Note**
Apache's mod_php module does not work with worker nor event MPM: "Apache is running a threaded MPM, but your PHP Module is not compiled to be threadsafe."

## Testing

Just execute __run.sh__ file and wait until log files are created for every service. Script runs every service and tests all PHP files placed in __www__ directory. Then results can found under __results__ directory and this is part of one:

    apache_prefork-mod_php_1  | # Testing 05_triggererror.php
    apache_prefork-mod_php_1  | [Sat Jul 14 11:18:08.262572 2018] [php7:error] [pid 18] [client 127.0.0.1:33814] PHP Fatal error:  Cannot divide by zero in /srv/http/05_triggererror.php on line 2
    apache_prefork-mod_php_1  | 127.0.0.1 - - [14/Jul/2018:11:18:08 +0000] "GET /05_triggererror.php HTTP/1.1" 500 -
    apache_prefork-mod_php_1  | ------------
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  | # Testing 06_haltcompiler.php
    apache_prefork-mod_php_1  | 127.0.0.1 - - [14/Jul/2018:11:18:09 +0000] "GET /06_haltcompiler.php HTTP/1.1" 200 -
    apache_prefork-mod_php_1  | ------------
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  | # Testing 07_selfkill.php
    apache_prefork-mod_php_1  | ------------
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  |
    apache_prefork-mod_php_1  | # Testing 08_selfkillforce.php
    apache_prefork-mod_php_1  | ------------

As you can see there is no trace for __07_selfkill.php__ and __08_selfkillforce.php__ for PHP running embedded in Apache running with prefork MPM. However there was not found any other possibility to bypass logging.

FROM archimg/base-devel

# Install Apache server, PHP module, wget and supervisor
RUN pacman -Syy && \
    pacman -S --noconfirm apache php-apache wget supervisor

# Fix ServerName error
RUN echo -e "ServerName localhost\n" >> /etc/httpd/conf/httpd.conf

# Set prefork MPM
RUN sed -ri 's/^LoadModule mpm_event_module modules\/mod_mpm_event.so$/LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/g' /etc/httpd/conf/httpd.conf

# Enable PHP module
RUN echo -e "LoadModule php7_module modules/libphp7.so\n" >> /etc/httpd/conf/httpd.conf \
    && echo -e "AddHandler php7-script php\n" >> /etc/httpd/conf/httpd.conf \
    && echo -e "Include conf/extra/php7_module.conf\n" >> /etc/httpd/conf/httpd.conf

# Redirect logs to stdout / stderr
RUN ln -sfT /dev/stderr "/var/log/httpd/error_log" \
    && ln -sfT /dev/stdout "/var/log/httpd/access_log"

EXPOSE 80

CMD ["/usr/bin/supervisord", "-n", "-uroot", "-ecritical"]

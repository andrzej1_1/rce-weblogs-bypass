#!/usr/bin/env bash

# Some sleep to make sure server is started
sleep 3

# Test all files
cd /srv/http/

echo -e "Testing started\n\n"
for i in *.php; do
    [ -f "$i" ] || break
    printf "\n\n# Testing $i\n"
    wget -O /dev/null -q -t 1 "http://127.0.0.1/$i"
    echo -e "------------\n\n"
    sleep 1 # give some time for fpm or fcgi to restart
done
echo -e "All files has been tested!\n\n"
